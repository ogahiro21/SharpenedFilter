# Sharpend Filter

## About Sharpend filter
ラプラシアンフィルタによりエッジを強調するフィルタ。  
$`\delta^{2} f = 5f[i,j] - (f[i-1,j]+f[i+1,j]+f[i,j-1]+f[i,j+1])`$  

- 入力画像  
![in](https://gitlab.com/ogahiro21/SharpenedFilter/raw/filtering/image/in14.jpeg)

- 出力画像  
![out](https://gitlab.com/ogahiro21/SharpenedFilter/raw/filtering/image/out14.jpeg)  

## Usage
**コンパイル**
```
gcc -o cpbmp sharpend_filter.c bmpfile.o -lm
```
**画像の出力**
```
./cpbmp in14.bmp ans14.bmp
```
